#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'

class GenerateQAJobs
  def initialize(no_of_example_files)
    @no_of_example_files = no_of_example_files
    puts "no_of_example_files: #{@no_of_example_files}"
  end

  # rubocop:disable Metrics/AbcSize
  def execute
    jobs = load_yml_contents('base')
    jobs.merge!(load_yml_contents('sanity_framework'))
    jobs.merge!(load_yml_contents('custom_parallel'))
    jobs.merge!(load_yml_contents('instance', should_automatically_run?('test_instance_all')))
    jobs.merge!(load_yml_contents('relative_url', should_automatically_run?('test_instance_all')))
    jobs.merge!(load_yml_contents('decomposition_single_db', should_automatically_run?('test_instance_all')))
    jobs.merge!(load_yml_contents('decomposition_multiple_db', should_automatically_run?('test_instance_all')))
    jobs.merge!(load_yml_contents('repository_storage', should_automatically_run?('test_instance_all_repository_storage')))
    jobs.merge!(load_yml_contents('omnibus_image'))
    jobs.merge!(load_yml_contents('update', should_automatically_run?('test_instance_all')))
    jobs.merge!(load_yml_contents('omnibus_upgrade'))
    jobs.merge!(load_yml_contents('ee_previous_to_ce_update'))
    jobs.merge!(load_yml_contents('mattermost', should_automatically_run?('test_integration_mattermost')))
    jobs.merge!(load_yml_contents('service_ping_disabled', should_automatically_run?('test_integration_servicepingdisabled')))
    jobs.merge!(load_yml_contents('ldap_no_tls', should_automatically_run?('test_integration_ldapnotls')))
    jobs.merge!(load_yml_contents('ldap_tls', should_automatically_run?('test_integration_ldaptls')))
    jobs.merge!(load_yml_contents('ldap_no_server', should_automatically_run?('test_integration_ldapnoserver')))
    jobs.merge!(load_yml_contents('instance_saml', should_automatically_run?('test_integration_instancesaml')))
    jobs.merge!(load_yml_contents('group_saml', should_automatically_run?('test_integration_groupsaml')))
    jobs.merge!(load_yml_contents('object_storage', should_automatically_run?('test_instance_all_object_storage')))
    jobs.merge!(load_yml_contents('object_storage_aws', should_automatically_run?('test_instance_all_object_storage')))
    jobs.merge!(load_yml_contents('object_storage_gcs', should_automatically_run?('test_instance_all_object_storage')))
    jobs.merge!(load_yml_contents('object_storage_registry_tls', should_automatically_run?('test_integration_registrytls')))
    jobs.merge!(load_yml_contents('registry', should_automatically_run?('test_integration_registry')))
    jobs.merge!(load_yml_contents('packages', should_automatically_run?('test_instance_all_packages')))
    jobs.merge!(load_yml_contents('elasticsearch', should_automatically_run?('test_integration_elasticsearch')))
    jobs.merge!(load_yml_contents('praefect', should_automatically_run?('test_instance_all')))
    jobs.merge!(load_yml_contents('gitaly_cluster', should_automatically_run?('test_instance_all')))
    jobs.merge!(load_yml_contents('mtls', should_automatically_run?('test_instance_all_mtls')))
    jobs.merge!(load_yml_contents('smtp', should_automatically_run?('test_integration_smtp')))
    jobs.merge!(load_yml_contents('jira', should_automatically_run?('test_instance_all_jira')))
    jobs.merge!(load_yml_contents('integrations', should_automatically_run?('test_instance_all_integrations')))
    jobs.merge!(load_yml_contents('large_setup', should_automatically_run?('test_instance_all_can_use_large_setup')))
    jobs.merge!(load_yml_contents('cloud_activation', should_automatically_run?('test_instance_all_cloud_activation')))
    jobs.merge!(load_yml_contents('registry_with_cdn', should_automatically_run?('test_integration_registrywithcdn')))
    jobs.merge!(load_yml_contents('staging'))

    # Disabling geo jobs temporarily due to https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/774
    # jobs.merge!(load_yml_contents('geo', should_automatically_run?('scenario_test_geo')))

    yaml_string = jobs.to_yaml
    de_stringify_reference(yaml_string)

    yaml_string
  end

  # rubocop:enable Metrics/AbcSize

  private

  def should_automatically_run?(example_file_name)
    @no_of_example_files.include?(example_file_name)
  end

  def load_yml_contents(file_prefix, automatic = true)
    jobs_dir_path = File.expand_path('../.gitlab/ci/jobs', __dir__)
    file_contents = File.read(File.join(jobs_dir_path, "#{file_prefix}.gitlab-ci.yml"))

    # Enclose !reference in double quotes at it is not supported with YAML.load
    stringify_reference(file_contents)
    yaml_hash = YAML.safe_load(file_contents)

    yaml_hash.each { |key, value| value.merge!("when" => "manual") } unless automatic

    yaml_hash
  end

  def stringify_reference(str)
    match_data = str.match(/!reference \[[a-zA-Z_, ]*\]/)
    str.gsub!(match_data[0], "\"#{match_data[0]}\"") if match_data
  end

  def de_stringify_reference(str)
    match_data = str.match(/"(!reference \[[a-zA-Z_, ]*\])"/)
    str.gsub!(match_data[0], match_data[1]) if match_data
  end
end

jobs = GenerateQAJobs.new(Dir.glob('no_of_examples/*').map { |s| File.basename(s, '.*') }).execute

File.open('generated-qa-jobs.yml', 'w') { |f| f.write(jobs) }
