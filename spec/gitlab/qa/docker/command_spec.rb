# frozen_string_literal: true

describe Gitlab::QA::Docker::Command do
  let(:docker) { spy('docker') }

  before do
    stub_const('Gitlab::QA::Docker::Shellout', docker)
  end

  describe '#<<' do
    it 'appends command arguments' do
      subject << '--help'

      expect(subject.args).to include '--help'
    end

    it 'returns self' do
      expect(subject << 'args').to eq subject
    end
  end

  describe '#volume' do
    it 'appends volume arguments' do
      subject.volume('/from', '/to', 'Z')

      expect(subject.to_s).to include '--volume /from:/to:Z'
    end
  end

  describe '#env' do
    it 'appends env arguments with quotes' do
      subject.env('TEST', 'some value')

      expect(subject.to_s).to include '--env TEST="some value"'
    end
  end

  describe '#mask_secrets' do
    context 'when masking one secret' do
      subject { described_class.new('command with secret password', mask_secrets: 'password') }

      it 'masks specified secret string' do
        expect(subject.mask_secrets).to eq('docker command with secret *****')
      end
    end

    context 'when masking multiple secrets' do
      subject { described_class.new('command with secret password', mask_secrets: %w[password secret]) }

      it 'masks specified secret strings' do
        expect(subject.mask_secrets).to eq('docker command with ***** *****')
      end
    end
  end

  describe 'execute!' do
    it 'calls docker engine shellout' do
      expect(docker).to receive(:execute!)

      subject.execute!
    end
  end

  describe '.execute' do
    it 'executes command directly' do
      instance = double('command')
      expect(instance).to receive(:execute!)
      allow(described_class).to receive(:new).and_return(instance)

      described_class.execute('version')
    end
  end
end
