# frozen_string_literal: true

module Gitlab
  module QA
    describe Component::Gitlab do
      before do
        Runtime::Scenario.define(:omnibus_configuration, Runtime::OmnibusConfiguration.new)
        Runtime::Scenario.define(:seed_db, false)
        Runtime::Scenario.define(:seed_admin_token, true)
        Runtime::Scenario.define(:omnibus_exec_commands, [])
        Runtime::Scenario.define(:skip_server_hooks, true)
      end

      let(:full_ce_address) { 'registry.gitlab.com/foo/gitlab/gitlab-ce' }
      let(:full_ce_address_with_complex_tag) { "#{full_ce_address}:omnibus-7263a2" }

      describe '#release' do
        context 'with no release' do
          it 'defaults to CE' do
            expect(subject.release.to_s).to eq 'gitlab/gitlab-ce:nightly'
          end
        end
      end

      describe '#release=' do
        before do
          subject.release = release
        end

        context 'when release is a Release object' do
          let(:release) { create_release('CE') }

          it 'returns a correct release' do
            expect(subject.release.to_s).to eq 'gitlab/gitlab-ce:nightly'
          end
        end

        context 'when release is a string' do
          context 'with a simple tag' do
            let(:release) { full_ce_address_with_complex_tag }

            it 'returns a correct release' do
              expect(subject.release.to_s).to eq full_ce_address_with_complex_tag
            end
          end
        end
      end

      describe '#name' do
        before do
          subject.release = create_release('EE')
        end

        it 'returns a unique name' do
          expect(subject.name).to match(/\Agitlab-ee-(\w+){8}\z/)
        end
      end

      describe '#hostname' do
        it { expect(subject.hostname).to match(/\Agitlab-ce-(\w+){8}\.\z/) }

        context 'with a network' do
          before do
            subject.network = 'local'
          end

          it 'returns a valid hostname' do
            expect(subject.hostname).to match(/\Agitlab-ce-(\w+){8}\.local\z/)
          end
        end
      end

      describe '#address' do
        context 'with a network' do
          before do
            subject.network = 'local'
          end

          it 'returns a HTTP address' do
            expect(subject.address)
              .to match(%r{http://gitlab-ce-(\w+){8}\.local\z})
          end
        end
      end

      describe '#start' do
        let(:docker) { spy('docker command') }

        before do
          stub_const('Gitlab::QA::Docker::Command', docker)

          allow(subject).to receive(:ensure_configured!)
        end

        it 'runs a docker command' do
          subject.start

          expect(docker).to have_received(:execute!)
        end

        it 'should dynamically bind HTTP port' do
          subject.start

          expect(docker).to have_received(:<<).with('-d -p 80')
        end

        it 'should specify the name' do
          subject.start

          expect(docker).to have_received(:<<)
                              .with("--name #{subject.name}")
        end

        it 'should specify the hostname' do
          subject.start

          expect(docker).to have_received(:<<)
                              .with("--hostname #{subject.hostname}")
        end

        it 'bind-mounds volume with logs in an appropriate directory' do
          allow(Gitlab::QA::Runtime::Env)
            .to receive(:host_artifacts_dir)
                  .and_return('/tmp/gitlab-qa/gitlab-qa-run-2018-07-11-10-00-00-abc123')

          subject.name = 'my-gitlab'

          subject.start

          expect(docker).to have_received(:volume)
                              .with('/tmp/gitlab-qa/gitlab-qa-run-2018-07-11-10-00-00-abc123/my-gitlab/logs', '/var/log/gitlab', 'Z')
        end

        context 'with a network' do
          before do
            subject.network = 'testing-network'
          end

          it 'should specify the network' do
            subject.start

            expect(docker).to have_received(:<<)
                                .with('--net testing-network')
          end
        end

        context 'with volumes' do
          before do
            subject.volumes = { '/from' => '/to' }
          end

          it 'adds --volume switches to the command' do
            subject.start

            expect(docker).to have_received(:volume)
                                .with('/from', '/to', 'Z')
          end
        end

        context 'with environment' do
          before do
            subject.environment = { 'TEST' => 'some value' }
          end

          it 'adds environment variables to the command' do
            subject.start

            expect(docker).to have_received(:env)
                                .with('TEST', 'some value')
          end
        end

        context 'with network_alias' do
          before do
            subject.add_network_alias('lolcathost')
          end

          it 'adds --network-alias switches to the command' do
            subject.start

            expect(docker).to have_received(:<<)
                                .with('--network-alias lolcathost')
          end
        end
      end

      describe '#seed_db' do
        let(:docker_engine) { spy('docker engine') }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
          Runtime::Scenario.define(:seed_admin_token, false)
        end

        context 'when `--seed-db` is set' do
          shared_examples 'exec docker commands when instance is ready' do
            let(:tmp_dir) { File.expand_path('./tmp', __dir__) }

            before do
              stub_const('Gitlab::QA::Component::Gitlab::DATA_SEED_PATH', tmp_dir)

              Dir.mkdir(tmp_dir)
              FileUtils.touch("#{tmp_dir}/test_file1.rb")
              FileUtils.touch("#{tmp_dir}/test_file2.rb")
              FileUtils.touch("#{tmp_dir}/file3.rb")
            end

            after do
              FileUtils.rm_rf(tmp_dir)
            end

            it 'executes given datat seed script' do
              allow(Runtime::Scenario).to receive(:seed_db).and_return(file_patterns)

              subject.process_exec_commands

              expect(docker_engine).to have_received(:copy)
              expect(docker_engine).to have_received(:exec).exactly(number_of_files).times
            end
          end

          context 'with duplicated search pattern' do
            let(:file_patterns) { ['test*.rb', 'test_file1.rb'] }
            let(:number_of_files) { 2 }

            it_behaves_like 'exec docker commands when instance is ready'
          end

          context 'with all seed scripts' do
            let(:file_patterns) { ['*'] }
            let(:number_of_files) { 3 }

            it_behaves_like 'exec docker commands when instance is ready'
          end

          context 'without matches' do
            let(:file_patterns) { ['test_file1'] }
            let(:number_of_files) { 0 }

            it_behaves_like 'exec docker commands when instance is ready'
          end
        end

        context 'when `--seed-db` is not set' do
          it 'does not exec docker commands' do
            subject.process_exec_commands

            expect(docker_engine).not_to have_received(:exec)
          end
        end
      end

      describe '#seed_admin_token' do
        let(:docker_engine) { spy('docker engine') }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
        end

        context 'when `--no-admin-token is set`' do
          before do
            Runtime::Scenario.define(:seed_admin_token, false)
          end

          it 'does not execute docker command' do
            subject.process_exec_commands

            expect(docker_engine).not_to have_received(:exec)
            expect(docker_engine).not_to have_received(:copy)
          end
        end

        context 'when `--no-admin-token is not set`' do
          context 'with default settings' do
            it 'copies and execute create token script' do
              subject.process_exec_commands

              expect(docker_engine).to have_received(:exec).once
              expect(docker_engine).to have_received(:copy)
            end
          end

          context 'when `seed_admin_token` set to false' do
            before do
              subject.seed_admin_token = false
            end

            it 'does not execute docker command' do
              subject.process_exec_commands

              expect(docker_engine).not_to have_received(:exec)
              expect(docker_engine).not_to have_received(:copy)
            end
          end
        end
      end

      describe '#teardown' do
        let(:docker_engine) { spy('docker engine') }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
        end

        context 'when `--no-teardown` is set' do
          it 'leaves containers running' do
            allow(subject).to receive(:teardown?).and_return(false)

            subject.teardown

            expect(docker_engine).not_to have_received(:stop)
            expect(docker_engine).not_to have_received(:remove)
            expect(docker_engine).to have_received(:ps)
          end
        end

        context 'when `--no-teardown` is not set' do
          it 'stops and removes containers' do
            allow(subject).to receive(:teardown?).and_return(true)

            subject.teardown

            expect(docker_engine).to have_received(:remove)
            expect(docker_engine).not_to have_received(:ps)
          end
        end
      end

      describe '#serverhooks' do
        let(:docker_engine) { spy('docker engine') }

        before do
          stub_const('Gitlab::QA::Docker::Engine', docker_engine)
          Runtime::Scenario.define(:seed_admin_token, false)
        end

        context 'when `--skip-server-hooks` is set' do
          before do
            Runtime::Scenario.define(:skip_server_hooks, true)
          end

          it 'does not add git server hooks to the gitlab container' do
            subject.process_exec_commands

            expect(docker_engine).not_to have_received(:exec)
          end
        end

        context 'when `--skip-server-hooks` is not set' do
          before do
            Runtime::Scenario.define(:skip_server_hooks, false)
          end

          it 'adds git server hooks to the gitlab container' do
            subject.process_exec_commands

            expect(docker_engine).to have_received(:exec).exactly(3).times
          end
        end
      end

      describe '#reconfigure' do
        let(:docker) { spy('docker') }

        before do
          stub_const('Gitlab::QA::Docker::Shellout', docker)
          Runtime::Scenario.define(:omnibus_configuration, Runtime::OmnibusConfiguration.new)
        end

        it 'configures omnibus by writing gitlab.rb' do
          subject.reconfigure

          expect(docker).to have_received(:new)
            .with(eq("docker exec #{subject.name} bash -c \"echo \\\"#{Runtime::Scenario.omnibus_configuration.to_s.gsub('"', '\\"')}\\\" > /etc/gitlab/gitlab.rb;\""))
        end
      end

      describe '#copy_key_file' do
        let(:docker) { spy('docker') }

        around do |example|
          ClimateControl.modify(MY_KEY: 'key') { example.run }
        end

        it 'copies a key file' do
          file_path = subject.copy_key_file('MY_KEY')

          stub_const('Gitlab::QA::Docker::Command', docker)
          allow(subject).to receive(:ensure_configured!)

          subject.start

          expect(docker).to have_received(:volume)
                                .with(file_path, file_path, 'Z')
        end
      end

      private

      def create_release(release)
        Release.new(release)
      end
    end
  end
end
