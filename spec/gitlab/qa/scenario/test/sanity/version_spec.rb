# frozen_string_literal: true

require 'json'

module Gitlab
  module QA
    module Scenario
      module Test
        module Sanity
          RSpec.describe Version do
            let(:mock_sha) { 'abcde12345' }
            let(:twenty_three_hours_ago) { (Time.now - 23 * 60 * 60).strftime('%FT%T') }
            let(:twenty_five_hours_ago) { (Time.now - 25 * 60 * 60).strftime('%FT%T') }
            let(:twenty_three_hours_ago_json) { { 'created_at' => twenty_three_hours_ago } }
            let(:twenty_five_hours_ago_json) { { 'created_at' => twenty_five_hours_ago } }
            let(:ce_string) { 'gitlab-org/gitlab-foss' }
            let(:ee_string) { 'gitlab-org/gitlab' }
            let(:dev_ee_string) { 'gitlab/gitlab-ee' }
            let(:main_ee_repo) { 'git@gitlab.com:gitlab-org/gitlab.git' }
            let(:main_ce_repo) { 'git@gitlab.com:gitlab-org/gitlab-foss.git' }
            let(:dev_ee_repo) { 'git@dev.gitlab.org:gitlab/gitlab-ee.git' }
            let(:host) { 'gitlab.com' }
            let(:dev_host) { 'dev.gitlab.org' }

            before do
              QA::Runtime::Scenario.define(:omnibus_configuration, QA::Runtime::OmnibusConfiguration.new)
              Runtime::Scenario.define(:seed_db, false)
              Runtime::Scenario.define(:seed_admin_token, true)
              Runtime::Scenario.define(:omnibus_exec_commands, [])
            end

            RSpec.shared_examples "weekday_hours" do |weekday_string, hours_int|
              let(:component_under_test) { described_class.new }

              context "with #{weekday_string}" do
                subject { component_under_test.send(:weekday_hours, weekday_string) }

                it { is_expected.to eq(hours_int) }
              end
            end

            describe '#perform' do
              it 'throws an error with incorrect version specified' do
                expect { described_class.perform('foo') }.to raise_error(Gitlab::QA::Release::InvalidImageNameError, "The release image name 'foo' does not have the expected format.")
              end

              context 'with a version' do
                before do
                  allow(Gitlab::QA::Component::Gitlab).to receive(:perform).and_return(version)
                end

                context 'with a ce release' do
                  let(:version) { { source: main_ce_repo, sha: mock_sha } }

                  it 'defaults to foss when no release specified' do
                    expect_any_instance_of(described_class).to receive(:api_commit_detail).with(host, ce_string, mock_sha).and_return(twenty_three_hours_ago_json)
                    described_class.perform
                  end

                  it 'passes when commit is less than 24 hours old' do
                    expect_any_instance_of(described_class).to receive(:api_commit_detail).with(host, ce_string, mock_sha).and_return(twenty_three_hours_ago_json)
                    expect_any_instance_of(described_class).to receive(:weekday_hours).with(twenty_three_hours_ago_json['created_at']).and_return(24)
                    expect { described_class.perform('ce') }.to output("Found commit #{mock_sha} in recent history of #{ce_string} on #{host}\n").to_stdout
                  end
                end

                context 'with an ee release' do
                  let(:version) { { source: main_ee_repo, sha: mock_sha } }

                  it 'fails when commit is more than 24 hours old' do
                    expect_any_instance_of(described_class).to receive(:api_commit_detail).with(host, ee_string, mock_sha).and_return(twenty_five_hours_ago_json)
                    expect_any_instance_of(described_class).to receive(:weekday_hours).with(twenty_five_hours_ago_json['created_at']).and_return(24)
                    expect do
                      expect { described_class.perform('ee') }.to output("Did not find #{mock_sha} in recent history of #{ee_string}\n").to_stdout
                    end.to raise_error(SystemExit)
                  end
                end

                context 'with a dev source' do
                  let(:mock_sha) { '12345abcde' }
                  let(:version) { { source: dev_ee_repo, sha: mock_sha } }

                  it 'finds commits from the dev repo' do
                    expect_any_instance_of(described_class).to receive(:api_commit_detail).with(dev_host, dev_ee_string, mock_sha).and_return(twenty_three_hours_ago_json)
                    expect_any_instance_of(described_class).to receive(:weekday_hours).with(twenty_three_hours_ago_json['created_at']).and_return(24)
                    expect { described_class.perform('ee') }.to output("Found commit #{mock_sha} in recent history of #{dev_ee_string} on #{dev_host}\n").to_stdout
                  end
                end
              end
            end

            describe '#weekday_hours' do
              # Monday to Sunday
              # This date is a Monday, returns 72 hours
              it_behaves_like "weekday_hours", "2021-02-01", 72
              # Tuesday to Saturday, return 24 hours
              it_behaves_like "weekday_hours", "2021-02-02", 24
              it_behaves_like "weekday_hours", "2021-02-03", 24
              it_behaves_like "weekday_hours", "2021-02-04", 24
              it_behaves_like "weekday_hours", "2021-02-05", 24
              it_behaves_like "weekday_hours", "2021-02-06", 24
              # This date is a Sunday, returns 48 hours
              it_behaves_like "weekday_hours", "2021-02-07", 48
            end

            describe '#api_commit_detail' do
              let(:component_under_test) { described_class.new }

              it 'performs a single API call' do
                test_uri = URI("https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab/repository/commits/#{mock_sha}")
                expect(Net::HTTP).to receive(:get).once.with(test_uri).and_return(JSON.generate(twenty_three_hours_ago_json))
                component_under_test.send(:api_commit_detail, 'gitlab.com', ee_string, mock_sha)
              end
            end

            describe '#commit_within_hours?' do
              let(:component_under_test) { described_class.new }

              it 'twenty three hours with 24 hours returns true' do
                expect(component_under_test.send(:commit_within_hours?, twenty_three_hours_ago, 24)).to eq(true)
              end

              it 'twenty three hours with 22 hours returns false' do
                expect(component_under_test.send(:commit_within_hours?, twenty_three_hours_ago, 22)).to eq(false)
              end

              it 'twenty five hours with 24 hours returns false' do
                expect(component_under_test.send(:commit_within_hours?, twenty_five_hours_ago, 24)).to eq(false)
              end

              it 'twenty five hours with 26 hours returns true' do
                expect(component_under_test.send(:commit_within_hours?, twenty_five_hours_ago, 26)).to eq(true)
              end
            end
          end
        end
      end
    end
  end
end
