# frozen_string_literal: true

describe Gitlab::QA::Report::ReportResults do
  describe '#invoke!' do
    let(:test_case_project) { 'valid-test-case-project' }
    let(:results_issue_project) { 'valid-results-issue-project' }
    let(:test_file_full) { 'qa/specs/features/browser_ui/stage/test_spec.rb' }
    let(:test_file_partial) { 'browser_ui/stage/test_spec.rb' }
    let(:test_name) { 'test-name' }
    let(:title) { "#{test_file_partial} | #{test_name}" }
    let(:testcase_url) { 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/1269' }
    let(:issue_description) { "### Full description\n\n#{test_name}\n\n### File path\n\n#{test_file_full}" }
    let(:testcase_graph) do
      <<~MKDOWN.strip
      ### Executions

      All Environments:
      <img src="https://dashboards.quality.gitlab.net/render/d-solo/cW0UMgv7k/spec-health?orgId=1&var-run_type=All&var-name=#{test_name}&panelId=4&width=1000&height=500" />
      MKDOWN
    end

    let(:testcase_results) { "\n\n### DO NOT EDIT BELOW THIS LINE\n\nActive and historical test results:\n\n\n\nhttp://issue.url/-/issues/920" }
    let(:testcase_description) { "#{issue_description}\n\n#{testcase_graph}#{testcase_results}" }
    let(:base_labels) { ['Quality', 'devops::stage', 'status::automated'] }
    let(:issue_labels) { base_labels + ['Testcase Linked'] }
    let(:testcase) do
      Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description)
        .new('http://testcase.url/-/quality/test_cases/1', base_labels, 1, 'test_case', 'opened', title, testcase_description)
    end

    let(:issue) do
      Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description)
        .new('http://issue.url/-/issues/920', issue_labels, 920, 'issue', 'opened', title, issue_description)
    end

    context 'with valid input' do
      let(:test_file) { 'file.json' }
      let(:test_data) do
        <<~JSON
          {
            "examples": [
              {
                "full_description": "#{test_name}",
                "file_path": "#{test_file_full}",
                "testcase": "#{testcase_url}"
              }
            ]
          }
        JSON
      end

      subject { described_class.new(token: 'token', input_files: 'files', test_case_project: test_case_project, results_issue_project: results_issue_project) }

      before do
        allow(subject).to receive(:assert_input_files!)
        allow(subject.__send__(:gitlab)).to receive(:assert_user_permission!)
        allow(::Dir).to receive(:glob).and_return([test_file])
        allow(::File).to receive(:read).with(test_file).and_return(test_data)
      end

      context 'when a test case is linked in the given test' do
        it 'finds the test case by given URL' do
          expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase_by_iid).with(anything).and_return(testcase)
          expect(subject.__send__(:results_issue_project_reporter)).to receive(:find_linked_results_issue_by_iid).and_return(issue)
          expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
          expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

          expect { subject.invoke! }.to output.to_stdout
        end

        context 'when the test title has been changed' do
          let(:old_testcase_description) do
            <<~MKDOWN.strip
              ### Full description

              old-title

              ### File path

              #{test_file_full}

              ### Executions

              All Environments:
              <img src="https://dashboards.quality.gitlab.net/render/d-solo/cW0UMgv7k/spec-health?orgId=1&var-run_type=All&var-name=old-title&panelId=4&width=1000&height=500" />

              #{testcase_results}
            MKDOWN
          end

          let(:old_testcase) do
            Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description)
              .new('http://testcase.url/-/quality/test_cases/1', base_labels, 1, 'test_case', 'opened', "#{test_file_partial} | old-title", old_testcase_description)
          end

          it 'updates the test case title, description, and graph' do
            expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase_by_iid).with(anything).and_return(old_testcase)

            expect(::Gitlab).to receive(:edit_issue)
            .with(
              test_case_project,
              testcase.iid,
              hash_including(description: testcase_description, title: title))
            .and_return(testcase)

            expect(subject.__send__(:results_issue_project_reporter)).to receive(:get_related_issue).and_return(issue)
            expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
            expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

            expect { subject.invoke! }.to output.to_stdout
          end

          %w[canary production preprod release].each do |pipeline|
            context "with a #{pipeline} pipeline" do
              around do |example|
                ClimateControl.modify(
                  CI_PROJECT_NAME: pipeline,
                  TOP_UPSTREAM_SOURCE_JOB: 'https://ops.gitlab.net',
                  CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
              end

              it 'does not update the test case title' do
                expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase_by_iid).with(anything).and_return(old_testcase)
                expect(::Gitlab).not_to receive(:edit_issue)

                expect(subject.__send__(:results_issue_project_reporter)).to receive(:get_related_issue).and_return(issue)
                expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
                expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end
        end
      end

      context 'when a test case exists for a given test but is not linked' do
        let(:search_response) { Struct.new(:auto_paginate).new([testcase]) }
        let(:test_data) do
          <<~JSON
            {
              "examples": [
                {
                  "full_description": "test-name",
                  "file_path": "#{test_file_full}"
                }
              ]
            }
          JSON
        end

        before do
          allow(::File).to receive(:write)
        end

        it 'finds the test case by searching by the test file and name' do
          expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase_by_iid).and_return(nil)

          expect(::Gitlab).to receive(:issues)
            .with(test_case_project, { search: %("#{test_file_partial}" "test-name") })
            .and_return(search_response)

          expect(subject.__send__(:results_issue_project_reporter)).to receive(:get_related_issue)
          expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
          expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

          expect { subject.invoke! }.to output.to_stdout
        end

        it 'finds the test case and writes it back to the report' do
          expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(testcase)

          test_data_parsed = JSON.parse(test_data)
          example = test_data_parsed.dig('examples', 0)
          example['testcase'] = testcase.web_url

          expect(::File).to receive(:write).with('file.json', JSON.pretty_generate(test_data_parsed))

          expect(subject.__send__(:results_issue_project_reporter)).to receive(:get_related_issue).and_return(issue)
          expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
          expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

          expect { subject.invoke! }.to output.to_stdout
        end
      end

      context 'when a test case does not exist for a given test' do
        it 'creates a new test case' do
          expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(false)
          expect(::Gitlab).to receive(:create_issue)
            .with(
              test_case_project,
              title,
              hash_including(labels: base_labels, issue_type: 'test_case'))
            .and_return(testcase)

          expect(subject.__send__(:results_issue_project_reporter)).to receive(:get_related_issue)
          expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
          expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

          expect { subject.invoke! }.to output(/Created new test_case: #{testcase.web_url}/).to_stdout
        end
      end

      context 'when an issue exists for a given test' do
        context 'when the issue is linked in the test case' do
          it 'finds the issue via the link in the test case and updates the issue' do
            expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(testcase)
            expect(subject.__send__(:results_issue_project_reporter)).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
            expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
            expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_labels).with(issue, anything, anything).and_return(true)

            expect { subject.invoke! }.to output(/Issue updated/).to_stdout
          end

          context 'when the test title has been changed' do
            let(:test_name) { 'Updated Title' }
            let(:old_issue) do
              Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title)
                .new('http://issue.url', issue_labels, 0, 'issue', 'opened', "#{test_file_partial} | test-name ")
            end

            it 'updates the issue title' do
              expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(testcase)
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:find_linked_results_issue_by_iid).with(anything, anything).and_return(old_issue)
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue_title).with(old_issue, anything).and_return(issue)

              expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

              expect { subject.invoke! }.to output.to_stdout
            end

            %w[canary production preprod release].each do |pipeline|
              context "with a #{pipeline} pipeline" do
                around do |example|
                  ClimateControl.modify(
                    CI_PROJECT_NAME: pipeline,
                    TOP_UPSTREAM_SOURCE_JOB: 'https://ops.gitlab.net',
                    CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
                end

                it 'does not update the issue title' do
                  expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(testcase)
                  expect(subject.__send__(:results_issue_project_reporter)).to receive(:find_linked_results_issue_by_iid).with(anything, anything).and_return(old_issue)
                  expect(subject.__send__(:results_issue_project_reporter)).not_to receive(:update_issue_title)

                  expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
                  expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end
          end

          context 'when the issue is closed' do
            let(:new_description) { "#{testcase_description}\n\nhttp://issue.url/-/issues/921" }
            let(:closed_issue) do
              Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description)
                .new('http://issue.url/-/issues/920', issue_labels, 920, 'issue', 'closed', title, issue_description)
            end

            let(:updated_testcase) do
              Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description)
                .new('http://testcase.url/-/quality/test_cases/1', base_labels, 1, 'test_case', 'opened', title, new_description)
            end

            let(:new_issue) do
              Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description)
                .new('http://issue.url/-/issues/921', issue_labels, 921, 'issue', 'opened', title, issue_description)
            end

            it 'creates a new issue and updates the test case with the new issue link' do
              expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(testcase)
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:issue_iid_from_testcase).with(testcase).and_return(anything)
              expect(::Gitlab).to receive(:issue).with(results_issue_project, anything).and_return(closed_issue)
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:find_issue).and_return(nil)

              expect(::Gitlab).to receive(:create_issue)
                .with(
                  results_issue_project,
                  title,
                  hash_including(description: issue_description, issue_type: "issue", labels: base_labels))
                .and_return(new_issue)

              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  test_case_project,
                  testcase.iid,
                  hash_including(description: new_description))
                .and_return(updated_testcase)

              expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

              expect { subject.invoke! }
                .to output(/No valid issue link found\nCreated new issue: #{new_issue.web_url}\nAdded results issue #{new_issue.web_url} link to test case #{testcase.web_url}/).to_stdout
            end
          end
        end

        context 'when the issue is not linked in the test case' do
          let(:search_response) { Struct.new(:auto_paginate).new([issue]) }
          let(:unlinked_testcase_description) { "#{issue_description}\n\n#{testcase_graph}\n\n### DO NOT EDIT BELOW THIS LINE\n\nActive and historical test results:\n\n" }
          let(:unlinked_testcase) do
            Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description)
              .new('http://testcase.url/-/quality/test_cases/1', base_labels, 1, 'test_case', 'opened', title, unlinked_testcase_description)
          end

          it 'finds the issue by searching by the test file and name and updates the test case with issue link' do
            expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(unlinked_testcase)
            expect(subject.__send__(:results_issue_project_reporter)).to receive(:find_linked_results_issue_by_iid).with(unlinked_testcase, anything).and_return(nil)
            expect(::Gitlab).to receive(:issues)
              .with(
                results_issue_project,
                { search: %("#{test_file_partial}" "#{test_name}") })
              .and_return(search_response)

            expect(::Gitlab).to receive(:edit_issue)
              .with(
                test_case_project,
                testcase.iid,
                hash_including(description: testcase_description))
              .and_return(testcase)

            expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
            expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

            expect { subject.invoke! }.to output(
              /No valid issue link found\nFound existing issue: #{issue.web_url}\nAdded results issue #{issue.web_url} link to test case #{testcase.web_url}/).to_stdout
          end

          context 'when the test name makes the title longer than the maximum 255 character' do
            let(:long_test_name) { 'x' * 255 }
            let(:name_truncated_to_fit_title) { 'x' * 220 }
            let(:test_data) do
              <<~JSON
                {
                  "examples": [
                    {
                      "full_description": "#{long_test_name}",
                      "file_path": "#{test_file_full}",
                      "testcase": "#{testcase_url}"
                    }
                  ]
                }
              JSON
            end

            let(:title) { "#{test_file_partial} | #{name_truncated_to_fit_title}..." }

            it 'finds the test case and issue with a truncated title' do
              expect(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(testcase)
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
              expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

              expect { subject.invoke! }.to output.to_stdout
            end
          end
        end
      end

      context 'when an issue does not exist for a given test' do
        let(:unlinked_testcase_description) { "#{issue_description}\n\n#{testcase_graph}\n\n### DO NOT EDIT BELOW THIS LINE\n\nActive and historical test results:\n\n" }
        let(:unlinked_testcase) do
          Struct.new(:web_url, :labels, :iid, :issue_type, :state, :title, :description)
            .new('http://testcase.url/-/quality/test_cases/1', base_labels, 1, 'test_case', 'opened', title, unlinked_testcase_description)
        end

        before do
          allow(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(unlinked_testcase)
          allow(subject.__send__(:results_issue_project_reporter)).to receive(:find_linked_results_issue_by_iid).with(unlinked_testcase, anything).and_return(nil)
          allow(subject.__send__(:results_issue_project_reporter)).to receive(:find_issue).and_return(nil)
        end

        it 'creates a new issue in the provided project' do
          expect(::Gitlab).to receive(:create_issue)
            .with(
              results_issue_project,
              title,
              hash_including(description: issue_description, issue_type: 'issue', labels: base_labels))
            .and_return(issue)

          expect(subject.__send__(:testcase_project_reporter)).to receive(:add_issue_link_to_testcase).with(unlinked_testcase, issue, anything)
          expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
          expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

          expect { subject.invoke! }
            .to output(/No valid issue link found\nCreated new issue: #{issue.web_url}/).to_stdout
        end

        it 'adds the new issue link to the test case' do
          expect(subject.__send__(:results_issue_project_reporter)).to receive(:create_issue).and_return(issue)

          expect(::Gitlab).to receive(:edit_issue)
            .with(
              test_case_project,
              testcase.iid,
              hash_including(description: testcase_description))
            .and_return(testcase)

          expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
          expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

          expect { subject.invoke! }
            .to output(/No valid issue link found\nAdded results issue #{issue.web_url} link to test case #{testcase.web_url}/).to_stdout
        end

        context 'when test is EE' do
          let(:test_file_full) { 'qa/specs/features/ee/browser_ui/stage/test_spec.rb' }

          it 'applies the ~"Enterprise Edition" label to test case and issue' do
            ClimateControl.modify(CI_PROJECT_NAME: 'staging') do
              expect(subject.__send__(:results_issue_project_reporter)).to receive(:create_issue).and_return(issue)
              expect(subject.__send__(:testcase_project_reporter)).to receive(:add_issue_link_to_testcase).with(unlinked_testcase, issue, anything)

              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  test_case_project,
                  testcase.iid,
                  hash_including(labels: %w[Quality devops::stage status::automated Enterprise\ Edition staging::passed]))
                .and_return(testcase)

              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  results_issue_project,
                  issue.iid,
                  hash_including(labels: %w[Quality devops::stage status::automated Testcase\ Linked Enterprise\ Edition staging::passed]))
                .and_return(issue)

              expect { subject.invoke! }.to output.to_stdout
            end
          end

          it 'removes ee from the path in the title but not the description' do
            expect(::Gitlab).to receive(:create_issue)
              .with(results_issue_project,
                    "browser_ui/stage/test_spec.rb | test-name",
                    hash_including(description: issue_description))
              .and_return(issue)

            expect(subject.__send__(:testcase_project_reporter)).to receive(:add_issue_link_to_testcase).with(unlinked_testcase, issue, anything)
            expect(subject.__send__(:testcase_project_reporter)).to receive(:update_testcase)
            expect(subject.__send__(:results_issue_project_reporter)).to receive(:update_issue)

            expect { subject.invoke! }.to output.to_stdout
          end
        end
      end

      context 'with an existing or new issue' do
        before do
          allow(subject.__send__(:testcase_project_reporter)).to receive(:find_testcase).with(anything).and_return(testcase)
          allow(subject.__send__(:results_issue_project_reporter)).to receive(:find_linked_results_issue_by_iid).with(testcase, anything).and_return(issue)
        end

        context 'with a passing test' do
          it 'adds a passed label' do
            ClimateControl.modify(CI_PROJECT_NAME: 'production') do
              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  test_case_project,
                  testcase.iid,
                  hash_including(labels: base_labels + ["production::passed"]))
                .and_return(testcase)

              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  results_issue_project,
                  issue.iid,
                  hash_including(labels: issue_labels + ["production::passed"]))
                .and_return(issue)

              expect { subject.invoke! }.to output.to_stdout
            end
          end

          it 'does not add a note' do
            expect(::Gitlab).to receive(:edit_issue).and_return(testcase)
            expect(::Gitlab).to receive(:edit_issue).and_return(issue)
            expect(::Gitlab).not_to receive(:create_issue_note)

            expect { subject.invoke! }.to output.to_stdout
          end

          context 'with an existing failed label' do
            let(:base_labels) { ['Quality', 'devops::stage', 'status::automated', 'staging::failed'] }

            it 'replaces the label' do
              ClimateControl.modify(CI_PROJECT_NAME: 'staging') do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    test_case_project,
                    testcase.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'staging::passed']))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    results_issue_project,
                    issue.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'Testcase Linked', 'staging::passed']))
                  .and_return(issue)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end
        end

        context 'with a failed test' do
          let(:error_class) { 'Error' }
          let(:test_data) do
            <<~JSON
              {
                "examples": [
                  {
                    "full_description": "#{test_name}",
                    "file_path": "#{test_file_full}",
                    "testcase": "#{testcase_url}",
                    "exceptions": [
                      {
                        "class": "#{error_class}",
                        "message": "An Error Here",
                        "message_lines": [ "Error line 1", "Error line 2 private_token=super-secret" ],
                        "backtrace": [ "Test Stacktrace" ]
                      }
                    ]
                  }
                ]
              }
            JSON
          end

          before do
            allow(::Gitlab).to receive(:edit_issue).and_return(testcase)
            allow(::Gitlab).to receive(:edit_issue).and_return(issue)
          end

          it 'adds a failed label' do
            ClimateControl.modify(CI_PROJECT_NAME: 'production') do
              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  test_case_project,
                  testcase.iid,
                  hash_including(labels: base_labels + ["production::failed"]))
                .and_return(testcase)

              expect(::Gitlab).to receive(:edit_issue)
                .with(
                  results_issue_project,
                  issue.iid,
                  hash_including(labels: issue_labels + ["production::failed"]))
                .and_return(issue)

              expect(subject.__send__(:results_issue_project_reporter)).to receive(:note_status)

              expect { subject.invoke! }.to output.to_stdout
            end
          end

          context 'when the error can be ignored' do
            let(:error_class) { 'Net::ReadTimeout' }

            it 'skips reporting' do
              expect(::Gitlab).not_to receive(:edit_issue)

              expect { subject.invoke! }
                .to output(/Issue update skipped because the error was Net::ReadTimeout/).to_stdout
            end
          end

          context 'with multiple errors' do
            let(:error_class) { 'Error' }
            let(:error_class2) { 'Error' }
            let(:test_data) do
              <<~JSON
                {
                  "examples": [
                    {
                      "full_description": "#{test_name}",
                      "file_path": "#{test_file_full}",
                      "testcase": "#{testcase_url}",
                      "exceptions": [
                        {
                          "class": "#{error_class}",
                          "message": "An Error Here",
                          "message_lines": [ "Error line 1", "Error line 2" ],
                          "backtrace": [ "Test Stacktrace" ]
                        },
                        {
                          "class": "#{error_class2}",
                          "message": "An Error Here",
                          "message_lines": [ "Error line 1", "Error line 2" ],
                          "backtrace": [ "Test Stacktrace" ]
                        }
                      ]
                    }
                  ]
                }
              JSON
            end

            context 'when all errors can be ignored' do
              let(:error_class) { 'Net::ReadTimeout' }
              let(:error_class2) { 'Net::ReadTimeout' }

              it 'skips reporting' do
                expect(::Gitlab).not_to receive(:edit_issue)

                expect { subject.invoke! }
                  .to output(/Issue update skipped because the errors were Net::ReadTimeout, Net::ReadTimeout/).to_stdout
              end
            end

            context 'when only one error can be ignored' do
              let(:error_class) { 'Net::ReadTimeout' }

              it 'does not skip reporting' do
                expect(::Gitlab).to receive(:edit_issue)
                expect(subject.__send__(:results_issue_project_reporter)).to receive(:note_status)

                expect { subject.invoke! }
                  .to output(/Issue updated/).to_stdout
              end
            end

            context 'when no error can be ignored' do
              it 'does not skip reporting' do
                expect(::Gitlab).to receive(:edit_issue)
                expect(subject.__send__(:results_issue_project_reporter)).to receive(:note_status)

                expect { subject.invoke! }
                  .to output(/Issue updated/).to_stdout
              end
            end
          end

          context 'when reporting for master/main pipelines' do
            around do |example|
              ClimateControl.modify(TOP_UPSTREAM_SOURCE_JOB: 'https://gitlab.com') { example.run }
            end

            it 'can report from gitlab-qa' do
              ClimateControl.modify(CI_PROJECT_NAME: 'gitlab-qa') do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    test_case_project,
                    testcase.iid,
                    hash_including(labels: base_labels + ["#{Gitlab::QA::Runtime::Env.default_branch}::failed"]))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    results_issue_project,
                    issue.iid,
                    hash_including(labels: issue_labels + ["#{Gitlab::QA::Runtime::Env.default_branch}::failed"]))
                  .and_return(issue)

                expect(subject.__send__(:results_issue_project_reporter)).to receive(:note_status)

                expect { subject.invoke! }.to output.to_stdout
              end
            end

            it 'can report from gitlab-qa-mirror' do
              ClimateControl.modify(CI_PROJECT_NAME: 'gitlab-qa-mirror') do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    test_case_project,
                    testcase.iid,
                    hash_including(labels: base_labels + ["#{Gitlab::QA::Runtime::Env.default_branch}::failed"]))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    results_issue_project,
                    issue.iid,
                    hash_including(labels: issue_labels + ["#{Gitlab::QA::Runtime::Env.default_branch}::failed"]))
                  .and_return(issue)

                expect(subject.__send__(:results_issue_project_reporter)).to receive(:note_status)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'with an existing passed label' do
            let(:base_labels) { ['Quality', 'devops::stage', 'status::automated', 'staging::passed'] }

            it 'replaces the label' do
              ClimateControl.modify(CI_PROJECT_NAME: 'staging') do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    test_case_project,
                    testcase.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'staging::failed']))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    results_issue_project,
                    issue.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'Testcase Linked', 'staging::failed']))
                  .and_return(issue)

                expect(subject.__send__(:results_issue_project_reporter)).to receive(:note_status)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'when reporting a specific job' do
            let(:failure_summary) { ":x: ~\"staging::failed\" in job `test-job` in http://job_url" }
            let(:note_content) { "#{failure_summary}\n\nError:\n```\nError: An Error Here\n```\n\nStacktrace:\n```\nError line 1\nError line 2 ********\nTest Stacktrace\n```\n" }
            let(:paginated_response) { Struct.new(:auto_paginate) }

            around do |example|
              ClimateControl.modify(
                CI_JOB_URL: 'http://job_url',
                CI_JOB_NAME: 'test-job',
                CI_PROJECT_NAME: 'staging'
              ) { example.run }
            end

            it 'adds a note that the test failed and a masked stack trace' do
              expect(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([]))
              expect(::Gitlab).to receive(:create_issue_note)
                .with(results_issue_project, issue.iid, note_content)

              expect { subject.invoke! }.to output.to_stdout
            end

            context 'with an existing discussion' do
              let(:existing_discussion) { Struct.new(:notes, :id).new(['body' => note_content], 0) }

              it 'adds a note to the discussion with no stack trace' do
                expect(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([existing_discussion]))
                expect(::Gitlab).to receive(:add_note_to_issue_discussion_as_thread)
                  .with('valid-results-issue-project', 920, 0, body: failure_summary)

                expect { subject.invoke! }.to output.to_stdout
              end

              context 'when the error or stack trace do not match' do
                let(:existing_discussion) do
                  Struct.new(:notes, :id)
                        .new(['body' => "#{failure_summary}\n\nError:\n```\nError: This time it's different\n```\n\nStacktrace:\n```\nAlso different\n```\n"], 0)
                end

                it 'adds a note as a new discussion' do
                  expect(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([existing_discussion]))
                  expect(::Gitlab).not_to receive(:add_note_to_issue_discussion_as_thread)
                  expect(::Gitlab).to receive(:create_issue_note)
                    .with(results_issue_project, issue.iid, note_content)

                  expect { subject.invoke! }.to output.to_stdout
                end
              end

              context 'with a different job name and environment' do
                around do |example|
                  ClimateControl.modify(
                    CI_JOB_URL: 'http://job_url',
                    CI_JOB_NAME: 'different-test-job',
                    CI_PROJECT_NAME: 'production'
                  ) { example.run }
                end

                it 'still matches the error and stack trace' do
                  expect(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([existing_discussion]))
                  expect(::Gitlab).to receive(:add_note_to_issue_discussion_as_thread)
                    .with('valid-results-issue-project', 920, 0, body: ":x: ~\"production::failed\" in job `different-test-job` in http://job_url")

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end

            context 'when the test is quarantined' do
              let(:testcase_quarantine_link) { "### Quarantine issue\n\nhttps://quarantine_issue/-/issues/338179" }
              let(:quarantined_testcase_description) { "#{issue_description}\n\n#{testcase_quarantine_link}\n\n#{testcase_graph}#{testcase_results}" }
              let(:note_content) { "#{failure_summary}\n\nError:\n```\nError: An Error Here\n```\n\nStacktrace:\n```\nError line 1\nError line 2\nTest Stacktrace\n```\n" }

              let(:test_data) do
                <<~JSON
                  {
                    "examples": [
                      {
                        "full_description": "#{test_name}",
                        "file_path": "#{test_file_full}",
                        "testcase": "#{testcase_url}",
                        "quarantine": {
                          "issue": "https://quarantine_issue/-/issues/338179",
                          "type": "bug"
                        },
                        "exceptions": [
                          {
                            "class": "Error",
                            "message": "An Error Here",
                            "message_lines": [ "Error line 1", "Error line 2" ],
                            "backtrace": [ "Test Stacktrace" ]
                          }
                        ]
                      }
                    ]
                  }
                JSON
              end

              before do
                allow(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([]))
              end

              it 'applies all quarantine labels and section' do
                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    test_case_project,
                    testcase.iid,
                    hash_including(labels: base_labels + ['quarantine', 'quarantine::bug', 'staging::failed']))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    test_case_project,
                    testcase.iid,
                    hash_including(description: quarantined_testcase_description))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    results_issue_project,
                    issue.iid,
                    hash_including(labels: issue_labels + ['quarantine', 'quarantine::bug', 'staging::failed']))
                  .and_return(issue)

                expect(::Gitlab).to receive(:create_issue_note)
                  .with(results_issue_project, issue.iid, note_content)

                expect { subject.invoke! }.to output.to_stdout
              end

              context 'when the issue already has the same labels and quarantine section' do
                let(:testcase_description) { "#{issue_description}\n\n#{testcase_quarantine_link}\n\n#{testcase_graph}#{testcase_results}" }
                let(:base_labels) { %w[Quality devops::stage status::automated staging::failed quarantine quarantine::bug] }

                it 'does not update the issue to change the labels or description' do
                  expect(::Gitlab).not_to receive(:edit_issue)
                  expect(::Gitlab).to receive(:create_issue_note)

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end

            context 'when a quarantined test is dequarantined' do
              let(:testcase_quarantine_link) { "### Quarantine issue\n\nhttps://quarantine_issue/-/issues/338179" }
              let(:testcase_description) { "#{issue_description}\n\n#{testcase_quarantine_link}\n\n#{testcase_graph}#{testcase_results}" }
              let(:dequarantined_testcase_description) { "#{issue_description}\n\n#{testcase_graph}#{testcase_results}" }

              let(:base_labels) { %w[Quality devops::stage status::automated staging::failed quarantine quarantine::bug] }
              let(:issue_labels) { %w[Quality devops::stage status::automated staging::failed quarantine quarantine::bug Testcase\ Linked] }

              it 'removes the quarantine label and section' do
                allow(::Gitlab).to receive(:issue_discussions).and_return(paginated_response.new([]))

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    test_case_project,
                    testcase.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'staging::failed']))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    test_case_project,
                    testcase.iid,
                    hash_including(description: dequarantined_testcase_description))
                  .and_return(testcase)

                expect(::Gitlab).to receive(:edit_issue)
                  .with(
                    results_issue_project,
                    issue.iid,
                    hash_including(labels: ['Quality', 'devops::stage', 'status::automated', 'Testcase Linked', 'staging::failed']))
                  .and_return(issue)

                expect(::Gitlab).to receive(:create_issue_note)
                  .with(results_issue_project, issue.iid, note_content)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end
        end

        context 'when an error occurs' do
          let(:post_to_slack) { double('Gitlab::QA::Slack::PostToSlack') }

          before do
            allow(post_to_slack).to receive(:invoke!)
          end

          around do |example|
            ClimateControl.modify(
              CI_SLACK_WEBHOOK_URL: 'http://webhook_url',
              QA_DEFAULT_BRANCH: 'master') { example.run }
          end

          %w[staging production preprod nightly release].each do |pipeline|
            context "with a #{pipeline} pipeline" do
              around do |example|
                ClimateControl.modify(
                  CI_PROJECT_NAME: pipeline,
                  TOP_UPSTREAM_SOURCE_JOB: 'https://ops.gitlab.net',
                  CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
              end

              it "posts a Slack message to qa-#{pipeline}" do
                allow(::Gitlab).to receive(:send).and_raise(StandardError)

                expect(Gitlab::QA::Slack::PostToSlack).to receive(:new).with(hash_including(channel: "qa-#{pipeline}")).and_return(post_to_slack).twice
                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          %w[gitlab-qa gitlab-qa-mirror].each do |pipeline|
            context "with a #{pipeline} pipeline" do
              around do |example|
                ClimateControl.modify(
                  CI_PROJECT_NAME: pipeline,
                  TOP_UPSTREAM_SOURCE_JOB: 'https://gitlab.com',
                  CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
              end

              it "posts a Slack message to qa-master" do
                allow(::Gitlab).to receive(:send).and_raise(StandardError)

                expect(Gitlab::QA::Slack::PostToSlack).to receive(:new).with(hash_including(channel: "qa-master")).and_return(post_to_slack).twice
                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'with a canary pipeline' do
            around do |example|
              ClimateControl.modify(CI_PROJECT_NAME: 'canary', CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
            end

            it 'posts a Slack message to qa-production' do
              allow(::Gitlab).to receive(:send).and_raise(StandardError)

              expect(Gitlab::QA::Slack::PostToSlack).to receive(:new).with(hash_including(channel: "qa-production")).and_return(post_to_slack).twice
              expect { subject.invoke! }.to output.to_stdout
            end
          end

          context 'with a staging-canary pipeline' do
            around do |example|
              ClimateControl.modify(CI_PROJECT_NAME: 'staging-canary', CI_COMMIT_REF_NAME: Gitlab::QA::Runtime::Env.default_branch) { example.run }
            end

            it 'posts a Slack message to qa-staging' do
              allow(::Gitlab).to receive(:send).and_raise(StandardError)

              expect(Gitlab::QA::Slack::PostToSlack).to receive(:new).with(hash_including(channel: "qa-staging")).and_return(post_to_slack).twice
              expect { subject.invoke! }.to output.to_stdout
            end
          end

          context 'when ref is not master' do
            around do |example|
              ClimateControl.modify(CI_COMMIT_REF_NAME: 'feature-branch') { example.run }
            end

            it 'reports the error and terminates without posting to Slack' do
              allow(::Gitlab).to receive(:send).and_raise(StandardError)

              expect(Gitlab::QA::Slack::PostToSlack).not_to receive(:new)
              expect { subject.invoke! }.to output.to_stdout
            end
          end

          context 'when a user permission error occurs' do
            it 'reports the error and terminates without posting to Slack' do
              stub_const("Gitlab::Error::NotFound", RuntimeError)

              allow(subject.__send__(:gitlab)).to receive(:assert_user_permission!).and_call_original
              allow(::Gitlab).to receive(:user).and_raise(Gitlab::Error::NotFound)

              expect(subject).not_to receive(:report_test)
              expect(Gitlab::QA::Slack::PostToSlack).not_to receive(:new)
              expect { subject.invoke! }
                .to output("You must have at least Maintainer access to the project to use this feature.\n").to_stderr
                .and raise_error(SystemExit)
            end
          end

          context 'when a timeout error occurs' do
            it 'retries up to 3 times before letting the error fail the job' do
              stub_const("Gitlab::QA::Report::GitlabIssueClient::RETRY_BACK_OFF_DELAY", 0.000001)

              allow(::Gitlab).to receive(:send).and_raise(Errno::ETIMEDOUT)

              expect(Gitlab::QA::Slack::PostToSlack).not_to receive(:new)

              expect { subject.invoke! }
                .to output(/Sleeping for .* seconds before retrying.../).to_stderr
                .and raise_error(Errno::ETIMEDOUT)
            end
          end
        end
      end
    end
  end
end
