# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '8.0.0'
  end
end
