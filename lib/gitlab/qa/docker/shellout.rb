# frozen_string_literal: true

require 'open3'
require 'active_support/core_ext/string/filters'

module Gitlab
  module QA
    module Docker
      class Shellout
        using Rainbow

        StatusError = Class.new(StandardError)

        def initialize(command)
          @command = command
          @output = []
          @logger = Runtime::Logger.logger
        end

        attr_reader :command, :output

        def execute! # rubocop:disable Metrics/AbcSize
          raise StatusError, 'Command already executed' if output.any?

          logger.info("Docker shell command: `#{command.mask_secrets.cyan}`")

          Open3.popen2e(@command.to_s) do |_in, out, wait|
            out.each do |line|
              output.push(line)

              if stream_progress
                print "."
              elsif command.stream_output
                puts line
              end

              yield line, wait if block_given?
            end
            puts if stream_progress && !output.empty?

            fail! if wait.value.exited? && wait.value.exitstatus.nonzero?

            logger.debug("Docker shell command output:\n#{string_output}") unless command.stream_output || output.empty?
          end

          string_output
        end

        private

        attr_reader :logger

        # Raise error and print output to error log level
        #
        # @return [void]
        def fail!
          logger.error("Docker shell command output:\n#{string_output}") unless command.stream_output
          raise StatusError, "Docker command `#{command.mask_secrets.truncate(100)}` failed! " + "✘".red
        end

        # Stream only command execution progress and log output when command finished
        #
        # @return [Boolean]
        def stream_progress
          !(Runtime::Env.ci || command.stream_output)
        end

        # Stringified command output
        #
        # @return [String]
        def string_output
          output.join.chomp
        end
      end
    end
  end
end
