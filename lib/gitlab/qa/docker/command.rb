# frozen_string_literal: true

module Gitlab
  module QA
    module Docker
      class Command
        attr_reader :args, :stream_output

        # Shell command
        #
        # @param [<String, Array>] cmd
        # @param [<String, Array>] mask_secrets
        # @param [Boolean] stream_output stream command output to stdout directly instead of logger
        def initialize(cmd = nil, mask_secrets: nil, stream_output: false)
          @args = Array(cmd)
          @mask_secrets = Array(mask_secrets)
          @stream_output = stream_output
        end

        def <<(*args)
          tap { @args.concat(args) }
        end

        def volume(from, to, opt = :z)
          tap { @args.push("--volume #{from}:#{to}:#{opt}") }
        end

        def name(identity)
          tap { @args.push("--name #{identity}") }
        end

        def env(name, value)
          tap { @args.push(%(--env #{name}="#{value}")) }
        end

        def to_s
          "docker #{@args.join(' ')}"
        end

        # Returns a masked string form of a Command
        #
        # @example
        #   Command.new('a docker command', mask_secrets: 'command').mask_secrets #=> 'a docker *****'
        #   Command.new('a docker command', mask_secrets: %w[docker command]).mask_secrets #=> 'a ***** *****'
        #
        # @return [String] The masked command string
        def mask_secrets
          @mask_secrets.each_with_object(+to_s) { |secret, s| s.gsub!(secret, '*****') }
        end

        def ==(other)
          to_s == other.to_s
        end

        def execute!(&block)
          Docker::Shellout.new(self).execute!(&block)
        rescue Docker::Shellout::StatusError => e
          e.set_backtrace([])

          raise e
        end

        def self.execute(cmd, mask_secrets: nil, &block)
          new(cmd, mask_secrets: mask_secrets).execute!(&block)
        end
      end
    end
  end
end
