# frozen_string_literal: true

require 'active_support/core_ext/object/blank'
require 'securerandom'

module Gitlab
  module QA
    module Runtime
      module Env
        extend self
        using Rainbow

        # Variables that are used in tests and are passed through to the docker container that executes the tests.
        # These variables should be listed in /docs/what_tests_can_be_run.md#supported-gitlab-environment-variables
        # unless they're defined elsewhere (e.g.: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
        ENV_VARIABLES = {
          'QA_IMAGE' => :qa_image,
          'QA_REMOTE_GRID' => :remote_grid,
          'QA_REMOTE_GRID_USERNAME' => :remote_grid_username,
          'QA_REMOTE_GRID_ACCESS_KEY' => :remote_grid_access_key,
          'QA_REMOTE_GRID_PROTOCOL' => :remote_grid_protocol,
          'QA_REMOTE_MOBILE_DEVICE_NAME' => :remote_mobile_device_name,
          'QA_REMOTE_TUNNEL_ID' => :remote_tunnel_id,
          'QA_BROWSER' => :browser,
          'QA_ADDITIONAL_REPOSITORY_STORAGE' => :qa_additional_repository_storage,
          'QA_PRAEFECT_REPOSITORY_STORAGE' => :qa_praefect_repository_storage,
          'QA_GITALY_NON_CLUSTER_STORAGE' => :qa_gitaly_non_cluster_storage,
          'QA_COOKIES' => :qa_cookie,
          'QA_DEBUG' => :qa_debug,
          'QA_DEFAULT_BRANCH' => :qa_default_branch,
          'QA_CAN_TEST_ADMIN_FEATURES' => :qa_can_test_admin_features,
          'QA_CAN_TEST_GIT_PROTOCOL_V2' => :qa_can_test_git_protocol_v2,
          'QA_CAN_TEST_PRAEFECT' => :qa_can_test_praefect,
          'QA_DISABLE_RSPEC_RETRY' => :qa_disable_rspec_retry,
          'QA_SIMULATE_SLOW_CONNECTION' => :qa_simulate_slow_connection,
          'QA_SLOW_CONNECTION_LATENCY_MS' => :qa_slow_connection_latency_ms,
          'QA_SLOW_CONNECTION_THROUGHPUT_KBPS' => :qa_slow_connection_throughput_kbps,
          'QA_GENERATE_ALLURE_REPORT' => :generate_allure_report,
          'QA_EE_ACTIVATION_CODE' => :ee_activation_code,
          'QA_EXPORT_TEST_METRICS' => :qa_export_test_metrics,
          'QA_INFLUXDB_URL' => :qa_influxdb_url,
          'QA_INFLUXDB_TOKEN' => :qa_influxdb_token,
          'QA_RUN_TYPE' => :qa_run_type,
          'QA_SKIP_PULL' => :qa_skip_pull,
          'QA_VALIDATE_RESOURCE_REUSE' => :qa_validate_resource_reuse,
          'GITLAB_API_BASE' => :api_base,
          'GITLAB_ADMIN_USERNAME' => :admin_username,
          'GITLAB_ADMIN_PASSWORD' => :admin_password,
          'GITLAB_USERNAME' => :user_username,
          'GITLAB_PASSWORD' => :user_password,
          'GITLAB_LDAP_USERNAME' => :ldap_username,
          'GITLAB_LDAP_PASSWORD' => :ldap_password,
          'GITLAB_FORKER_USERNAME' => :forker_username,
          'GITLAB_FORKER_PASSWORD' => :forker_password,
          'GITLAB_USER_TYPE' => :user_type,
          'GITLAB_SANDBOX_NAME' => :gitlab_sandbox_name,
          'GITLAB_QA_ACCESS_TOKEN' => :qa_access_token,
          'GITLAB_QA_ADMIN_ACCESS_TOKEN' => :qa_admin_access_token,
          'GITLAB_QA_DEV_ACCESS_TOKEN' => :qa_dev_access_token,
          'GITLAB_QA_CONTAINER_REGISTRY_ACCESS_TOKEN' => :qa_container_registry_access_token,
          'GITLAB_QA_ISSUE_URL' => :qa_issue_url,
          'GITHUB_ACCESS_TOKEN' => :github_access_token,
          'GITLAB_URL' => :gitlab_url,
          'SIMPLE_SAML_HOSTNAME' => :simple_saml_hostname,
          'SIMPLE_SAML_FINGERPRINT' => :simple_saml_fingerprint,
          'ACCEPT_INSECURE_CERTS' => :accept_insecure_certs,
          'EE_LICENSE' => :ee_license,
          'GCLOUD_ACCOUNT_EMAIL' => :gcloud_account_email,
          'GCLOUD_ACCOUNT_KEY' => :gcloud_account_key,
          'CLOUDSDK_CORE_PROJECT' => :cloudsdk_core_project,
          'GCLOUD_REGION' => :gcloud_region,
          'SIGNUP_DISABLED' => :signup_disabled,
          'GITLAB_CI_API_TOKEN' => :gitlab_ci_api_token,
          'GITLAB_QA_USERNAME_1' => :gitlab_qa_username_1,
          'GITLAB_QA_PASSWORD_1' => :gitlab_qa_password_1,
          'GITLAB_QA_USERNAME_2' => :gitlab_qa_username_2,
          'GITLAB_QA_PASSWORD_2' => :gitlab_qa_password_2,
          'GITHUB_USERNAME' => :github_username,
          'GITHUB_PASSWORD' => :github_password,
          'KNAPSACK_GENERATE_REPORT' => :knapsack_generate_report,
          'KNAPSACK_REPORT_PATH' => :knapsack_report_path,
          'KNAPSACK_TEST_FILE_PATTERN' => :knapsack_test_file_pattern,
          'KNAPSACK_TEST_DIR' => :knapsack_test_dir,
          'QA_KNAPSACK_REPORT_GCS_CREDENTIALS' => :qa_knapsack_report_gcs_credentials,
          'CI' => :ci,
          'CI_COMMIT_REF_NAME' => :ci_commit_ref_name,
          'CI_JOB_NAME' => :ci_job_name,
          'CI_JOB_URL' => :ci_job_url,
          'CI_JOB_TOKEN' => :ci_job_token,
          'CI_RUNNER_ID' => :ci_runner_id,
          'CI_SERVER_HOST' => :ci_server_host,
          'CI_SERVER_PERSONAL_ACCESS_TOKEN' => :ci_server_personal_access_token,
          'CI_NODE_INDEX' => :ci_node_index,
          'CI_NODE_TOTAL' => :ci_node_total,
          'CI_PROJECT_ID' => :ci_project_id,
          'CI_PROJECT_NAME' => :ci_project_name,
          'CI_SLACK_WEBHOOK_URL' => :ci_slack_webhook_url,
          'CI_PIPELINE_ID' => :ci_pipeline_id,
          'CI_PIPELINE_SOURCE' => :ci_pipeline_source,
          'CI_PIPELINE_URL' => :ci_pipeline_url,
          'CI_PIPELINE_CREATED_AT' => :ci_pipeline_created_at,
          'CI_MERGE_REQUEST_IID' => :ci_merge_request_iid,
          'CI_API_V4_URL' => :ci_api_v4_url,
          'GITLAB_CI' => :gitlab_ci,
          'ELASTIC_URL' => :elastic_url,
          'GITLAB_QA_LOOP_RUNNER_MINUTES' => :gitlab_qa_loop_runner_minutes,
          'MAILHOG_HOSTNAME' => :mailhog_hostname,
          'SLACK_QA_CHANNEL' => :slack_qa_channel,
          'SLACK_ICON_EMOJI' => :slack_icon_emoji,
          'GITLAB_QA_FORMLESS_LOGIN_TOKEN' => :gitlab_qa_formless_login_token,
          'GEO_MAX_FILE_REPLICATION_TIME' => :geo_max_file_replication_time,
          'GEO_MAX_DB_REPLICATION_TIME' => :geo_max_db_replication_time,
          'JIRA_HOSTNAME' => :jira_hostname,
          'JIRA_ADMIN_USERNAME' => :jira_admin_username,
          'JIRA_ADMIN_PASSWORD' => :jira_admin_password,
          'CACHE_NAMESPACE_NAME' => :cache_namespace_name,
          'DEPLOY_VERSION' => :deploy_version,
          'GITLAB_QA_USER_AGENT' => :gitlab_qa_user_agent,
          'GEO_FAILOVER' => :geo_failover,
          'GITLAB_INITIAL_ROOT_PASSWORD' => :initial_root_password,
          'GITLAB_TLS_CERTIFICATE' => :gitlab_tls_certificate,
          'AWS_S3_REGION' => :aws_s3_region,
          'AWS_S3_KEY_ID' => :aws_s3_key_id,
          'AWS_S3_ACCESS_KEY' => :aws_s3_access_key,
          'AWS_S3_BUCKET_NAME' => :aws_s3_bucket_name,
          'TOP_UPSTREAM_MERGE_REQUEST_IID' => :top_upstream_merge_request_iid,
          'GOOGLE_PROJECT' => :google_project,
          'GOOGLE_CLIENT_EMAIL' => :google_client_email,
          'GOOGLE_JSON_KEY' => :google_json_key,
          'GOOGLE_CDN_JSON_KEY' => :google_cdn_json_key,
          'GOOGLE_CDN_LB' => :google_cdn_load_balancer,
          'GOOGLE_CDN_SIGNURL_KEY' => :google_cdn_signurl_key,
          'GOOGLE_CDN_SIGNURL_KEY_NAME' => :google_cdn_signurl_key_name,
          'GCS_CDN_BUCKET_NAME' => :gcs_cdn_bucket_name,
          'GCS_BUCKET_NAME' => :gcs_bucket_name,
          'SMOKE_ONLY' => :smoke_only,
          'NO_ADMIN' => :no_admin,
          'CHROME_DISABLE_DEV_SHM' => :chrome_disable_dev_shm,
          'COLORIZED_LOGS' => :colorized_logs
        }.freeze

        ENV_VARIABLES.each do |env_name, method_name|
          attr_writer(method_name)

          define_method(method_name) do
            env_var_value_if_defined(env_name) || (if instance_variable_defined?("@#{method_name}")
                                                     instance_variable_get("@#{method_name}")
                                                   end)
          end
        end

        def variables
          defined_variables = ENV_VARIABLES.each_with_object({}) do |(name, attribute), vars|
            # Variables that are overridden in the environment take precedence
            # over the defaults specified by the QA runtime.
            value = env_var_name_if_defined(name) || send(attribute) # rubocop:disable GitlabSecurity/PublicSend
            vars[name] = value if value
          end
          qa_variables = ENV.each_with_object({}) do |(name, value), vars|
            next unless name.start_with?('QA_')

            var_name = env_var_name_if_defined(name)
            vars[name] = var_name if var_name
          end

          qa_variables.merge(defined_variables)
        end

        def debug?
          enabled?(ENV['QA_DEBUG'], default: true)
        end

        def log_level
          env_var_value_if_defined('QA_LOG_LEVEL')&.upcase || 'INFO'
        end

        def log_path
          env_var_value_if_defined('QA_LOG_PATH') || host_artifacts_dir
        end

        def default_branch
          env_var_value_if_defined('QA_DEFAULT_BRANCH') || 'main'
        end

        def gitlab_availability_timeout
          (env_var_value_if_defined('GITLAB_QA_AVAILABILITY_TIMEOUT') || 360).to_i
        end

        def gitlab_username
          env_var_value_if_defined('GITLAB_USERNAME') || 'gitlab-qa'
        end

        def gitlab_dev_username
          env_var_value_if_defined('GITLAB_DEV_USERNAME') || 'gitlab-qa-bot'
        end

        def gitlab_api_base
          env_var_value_if_defined('GITLAB_API_BASE') || 'https://gitlab.com/api/v4'
        end

        def pipeline_from_project_name
          ci_project_name.to_s.start_with?('gitlab-qa') ? QA::Runtime::Env.default_branch : ci_project_name
        end

        def run_id
          @run_id ||= "gitlab-qa-run-#{Time.now.strftime('%Y-%m-%d-%H-%M-%S')}-#{SecureRandom.hex(4)}"
        end

        def colorized_logs?
          enabled?(ENV['COLORIZED_LOGS'], default: false)
        end

        def dev_access_token_variable
          env_var_name_if_defined('GITLAB_QA_DEV_ACCESS_TOKEN')
        end

        def initial_root_password
          env_var_value_if_defined('GITLAB_INITIAL_ROOT_PASSWORD') || '5iveL!fe'
        end

        def deploy_environment
          env_var_value_if_defined('DEPLOY_ENVIRONMENT') || pipeline_from_project_name
        end

        def host_artifacts_dir
          @host_artifacts_dir ||= File.join(
            env_var_value_if_defined('QA_ARTIFACTS_DIR') || '/tmp/gitlab-qa', Runtime::Env.run_id
          )
        end

        def elastic_version
          env_var_value_if_defined('ELASTIC_VERSION') || '8.2.0'
        end

        def require_license!
          return if ENV.include?('EE_LICENSE')

          raise ArgumentError, 'GitLab License is not available. Please load a license into EE_LICENSE env variable.'
        end

        def require_no_license!
          return unless ENV.include?('EE_LICENSE')

          raise ArgumentError, "Unexpected EE_LICENSE provided. Please unset it to continue."
        end

        def require_qa_access_token!
          return unless env_var_value_if_defined('GITLAB_QA_ACCESS_TOKEN').to_s.strip.empty?

          raise ArgumentError, "Please provide GITLAB_QA_ACCESS_TOKEN"
        end

        def require_qa_dev_access_token!
          return unless env_var_value_if_defined('GITLAB_QA_DEV_ACCESS_TOKEN').to_s.strip.empty?

          raise ArgumentError, "Please provide GITLAB_QA_DEV_ACCESS_TOKEN"
        end

        def require_slack_qa_channel!
          return unless env_var_value_if_defined('SLACK_QA_CHANNEL').to_s.strip.empty?

          raise ArgumentError, "Please provide SLACK_QA_CHANNEL"
        end

        def require_ci_slack_webhook_url!
          return unless env_var_value_if_defined('CI_SLACK_WEBHOOK_URL').to_s.strip.empty?

          raise ArgumentError, "Please provide CI_SLACK_WEBHOOK_URL"
        end

        def require_kubernetes_environment!
          %w[GCLOUD_ACCOUNT_EMAIL GCLOUD_ACCOUNT_KEY CLOUDSDK_CORE_PROJECT].each do |env_key|
            unless ENV.key?(env_key)
              raise ArgumentError, "Environment variable #{env_key} must be set to run kubernetes specs"
            end
          end
        end

        def require_aws_s3_environment!
          %w[AWS_S3_REGION AWS_S3_KEY_ID AWS_S3_ACCESS_KEY AWS_S3_BUCKET_NAME].each do |env_key|
            unless ENV.key?(env_key)
              raise ArgumentError, "Environment variable #{env_key} must be set to run AWS S3 object storage specs"
            end
          end
        end

        def require_gcs_environment!
          %w[GOOGLE_PROJECT GOOGLE_CLIENT_EMAIL GOOGLE_JSON_KEY GCS_BUCKET_NAME].each do |env_key|
            unless ENV.key?(env_key)
              raise ArgumentError, "Environment variable #{env_key} must be set to run GCS object storage specs"
            end
          end
        end

        def require_gcs_with_cdn_environment!
          %w[GOOGLE_CDN_JSON_KEY GCS_CDN_BUCKET_NAME GOOGLE_CDN_LB GOOGLE_CDN_SIGNURL_KEY GOOGLE_CDN_SIGNURL_KEY_NAME].each do |env_key|
            unless ENV.key?(env_key)
              raise ArgumentError, "Environment variable #{env_key} must be set to run GCS with CDN enabled scenario"
            end
          end
        end

        def require_initial_password!
          return unless env_var_value_if_defined('GITLAB_INITIAL_ROOT_PASSWORD').to_s.strip.empty?

          raise(
            ArgumentError,
            "Environment variable GITLAB_INITIAL_ROOT_PASSWORD must be provided to set an initial root password."
          )
        end

        def require_ssh_tunnel!
          %w[TUNNEL_SSH_PRIVATE_KEY TUNNEL_SERVER_HOSTNAME TUNNEL_SSH_USER].each do |env_key|
            unless ENV.key?(env_key)
              raise ArgumentError, "Environment variable #{env_key} must be set to run tunnel specs"
            end
          end
        end

        def override_default_password!
          require_initial_password!

          ENV['GITLAB_PASSWORD'] = initial_root_password
          ENV['GITLAB_ADMIN_PASSWORD'] = initial_root_password
        end

        def skip_pull?
          enabled?(env_var_value_if_defined('QA_SKIP_PULL'), default: false)
        end

        def geo_failover?
          enabled?(env_var_value_if_defined('GEO_FAILOVER'), default: false)
        end

        def test_license_mode?
          enabled?(env_var_value_if_defined('TEST_LICENSE_MODE'), default: false)
        end

        def qa_export_test_metrics?
          enabled?(env_var_value_if_defined('QA_EXPORT_TEST_METRICS'), default: true)
        end

        def qa_run_type
          return env_var_value_if_defined('QA_RUN_TYPE') if env_var_value_valid?('QA_RUN_TYPE')

          live_envs = %w[staging staging-canary staging-ref canary preprod production]
          return unless live_envs.include?(ci_project_name)

          test_subset = if env_var_value_if_defined('NO_ADMIN') == 'true'
                          'sanity-no-admin'
                        elsif env_var_value_if_defined('SMOKE_ONLY') == 'true'
                          'sanity'
                        else
                          'full'
                        end

          "#{ci_project_name}-#{test_subset}"
        end

        private

        def enabled?(value, default: true)
          return default if value.nil?

          (value =~ /^(false|no|0)$/i) != 0
        end

        def env_var_value_valid?(variable)
          !ENV[variable].blank?
        end

        def env_var_value_if_defined(variable)
          return ENV[variable] if env_var_value_valid?(variable)
        end

        def env_var_name_if_defined(variable)
          # Pass through the variables if they are defined and not empty in the environment
          return "$#{variable}" if env_var_value_valid?(variable)
        end
      end
    end
  end
end
