# frozen_string_literal: true

require 'active_support/core_ext/enumerable'

module Gitlab
  module QA
    module Report
      module ResultsReporterShared
        TEST_CASE_RESULTS_SECTION_TEMPLATE = "\n\n### DO NOT EDIT BELOW THIS LINE\n\nActive and historical test results:"

        def find_issue(test)
          issues = search_for_issues(test)

          warn(%(Too many #{issue_type}s found with the file path "#{test.file}" and name "#{test.name}")) if issues.many?
          puts "Found existing #{issue_type}: #{issues.first.web_url}" unless issues.empty?

          issues.first
        end

        def find_issue_by_iid(iid)
          issues = gitlab.find_issues(iid: iid) do |issue|
            issue.state == 'opened' && issue.issue_type == issue_type
          end

          warn(%(#{issue_type} iid "#{iid}" not valid)) if issues.empty?

          issues.first
        end

        def issue_title_needs_updating?(issue, test)
          issue.title.strip != title_from_test(test) && !%w[canary production preprod release].include?(pipeline)
        end

        def new_issue_labels(test)
          ['Quality', "devops::#{test.stage}", 'status::automated']
        end

        def search_term(test)
          %("#{partial_file_path(test.file)}" "#{search_safe(test.name)}")
        end

        def up_to_date_labels(test:, issue: nil, new_labels: Set.new)
          labels = super
          labels |= new_issue_labels(test).to_set
          labels.delete_if { |label| label.start_with?("#{pipeline}::") }
          labels << (test.failures.empty? ? "#{pipeline}::passed" : "#{pipeline}::failed")
        end

        def update_issue_title(issue, test)
          old_title = issue.title.strip
          new_title = title_from_test(test)

          warn(%(#{issue_type} title needs to be updated from '#{old_title}' to '#{new_title}'))

          new_description = updated_description(issue, test)

          gitlab.edit_issue(iid: issue.iid, options: { title: new_title, description: new_description })
        end

        private

        def search_for_issues(test)
          gitlab.find_issues(options: { search: search_term(test) }) do |issue|
            issue.state == 'opened' && issue.issue_type == issue_type && issue.title.strip == title_from_test(test)
          end
        end
      end
    end
  end
end
