# frozen_string_literal: true

module Gitlab
  module QA
    module Component
      class Preprod < Staging
        ADDRESS = 'https://pre.gitlab.com'
      end
    end
  end
end
